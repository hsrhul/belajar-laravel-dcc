<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->title = 'user';
    }

    public function index()
    {
        $data = User::all();
        return view('admin.user.index', compact('data'));
    }
    public function create()
    {
        return view('admin.user.create');
    }
    public function store(Request $request)
    {
        $model = $request->all();
        $model['user_id'] = auth()->user()->id;
        $model['password'] = Hash::make($model['password']);
        $data = User::create($model);
        if($data){
            Alert::toast('Data Berhasil Disimpan', 'success');
        }else{
            Alert::toast('Data Berhasil Disimpan', 'danger');
        }
        return redirect('admin/user');
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = User::find($id);
        return view('admin.user.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $model = $request->all();
        $data = User::find($model['id']);
        $model['user_id'] = auth()->user()->id;
        if($model['password'] == null){
            $model['password'] = $model['password_skr'];
        }else{
            $model['password'] = Hash::make($model['password']);
        }
        if($data->update($model)){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/user');
    }

    public function destroy(Request $request, $id)
    {
        $data = User::find($id);
        if($data->delete()){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/user');
    }
}
