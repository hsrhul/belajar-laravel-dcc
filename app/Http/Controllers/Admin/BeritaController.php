<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Berita;
use Alert;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->title = 'berita';
    }

    public function index()
    {
        $data = Berita::all();
        return view('admin.berita.index', compact('data'));
    }
    public function create()
    {
        return view('admin.berita.create');
    }
    public function store(Request $request)
    {
        $model = $request->all();
        $model['user_id'] = auth()->user()->id;
        $data = Berita::create($model);
        if($data){
            Alert::toast('Data Berhasil Disimpan', 'success');
        }else{
            Alert::toast('Data Berhasil Disimpan', 'danger');
        }
        return redirect('admin/berita');
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Berita::find($id);
        return view('admin.berita.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $model = $request->all();
        $data = Berita::find($model['id']);
        $model['user_id'] = auth()->user()->id;
        if($data->update($model)){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/berita');
    }

    public function destroy(Request $request, $id)
    {
        $data = Berita::find($id);
        if($data->delete()){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/berita');
    }
}
