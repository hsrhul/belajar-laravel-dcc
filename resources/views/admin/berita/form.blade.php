@csrf
<!-- /.card-header -->
<div class="card-body">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" value="{{ isset($data->judul) ? $data->judul :
                null }}" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Kategori</label>
                <select class="form-control select2" style="width: 100%;" name="kategori">
                    @foreach(\App\Model\Kategori::all() as $v)
                        <option>{{$v->kategori}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea type="text" class="form-control" name="deskripsi" >
                    {{ isset($data->deskripsi) ? $data->deskripsi : null }}
                                </textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Isi</label>
                <textarea type="text" class="form-control" name="isi" >
                    {{ isset($data->isi) ? $data->isi : null }}
                                </textarea>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>