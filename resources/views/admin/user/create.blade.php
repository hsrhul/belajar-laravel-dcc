@extends('admin.index')

@push('head-tabel')
    @component('_card.head')
        Form Input Berita
    @endcomponent
@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Form</h3>
                </div>


                <form action="{{ route('user.store') }}" method="post">

                    @include('admin.user.form')

                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="{{ route('user.index') }}" class="btn btn-danger">Cancel</a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>

                </form>


            </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection