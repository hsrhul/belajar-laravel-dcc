@csrf
<!-- /.card-header -->
<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="name" value="{{ isset($data->name) ? $data->name :
                null }}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value="{{ isset($data->email) ?
                $data->email :
                null }}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" value="{{ isset($data->password) ?
                $data->password :
                null }}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Level</label>
                <select class="form-control" style="width: 100%;" name="level">
                    <option value="1" {{ (isset($model->level) ? $model->level : null)==1 ? 'selected="selected"' :  null }} > Super Admin</option>
                    <option value="2" {{ (isset($model->level) ? $model->level : null)==1 ? 'selected="selected"' :
                    null }} > Admin</option>
                    <option value="3" {{ (isset($model->level) ? $model->level : null)==1 ? 'selected="selected"' :
                    null }} > User</option>
                </select>

            </div>
        </div>
    </div>
    <!-- /.row -->
</div>